# BareGrid

## A flexible grid system in less than 3 Kb uncompressed!

## Supports

 - Chrome
 - Firefox 3+
 - Internet Explorer 8+
 - Opera 10+
 - Safari 4+

## Demo

A demo page is available [here][1].

  [1]: https://dl.dropbox.com/u/28039153/baregrid/demo.html